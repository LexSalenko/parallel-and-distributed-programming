import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class main {

    public static void main(String[] args) {
        try
        {
            Map<String, Long> map = Files.lines(Paths.get("E:\\Education\\4Course\\Параллельное и распределенное программирование\\LR1\\text.txt"))
                    .map(x -> x.replaceAll("\\p{P}", ""))
                    .map(String::toLowerCase)
                    .map(line -> line.split("\\s+"))
                    .flatMap(Arrays::stream)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            for (Map.Entry<String, Long> entry : map.entrySet()) {
                System.out.println(entry.getKey() + ": " + entry.getValue().toString());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }


}
