import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        try (Stream<Path> files = Files.walk(Paths.get("C:\\Thumbtack\\thumbtack_online_school_2018_2_aleksandr_salenko-master"))) {
            // traverse all files and sub-folders
            List<String> folderPaths = new ArrayList<>();
            files.map(Path::toAbsolutePath).forEach(path -> folderPaths.add(path.toString()));

            Map<String, List<String>> res = new HashMap<>();
            res.put("class", new ArrayList<>());
            res.put("interface", new ArrayList<>());
            res.put("enum", new ArrayList<>());

            Map<String, String> regExpMap = new HashMap();

            regExpMap.put("ClassRegExp", "(((|public|final|abstract|private|static|protected)(\\s+))?(class)(\\s+)(\\w+)(<.*>)?(\\s+extends\\s+\\w+)?(<.*>)?(\\s+implements\\s+)?(.*)?(<.*>)?(\\s*))\\{$");

            regExpMap.put("InterfaceRegExp", "(((|public|final|abstract|private|static|protected)(\\s+))?interface(\\s+)(\\w+)(<.*>)?(\\s+extends\\s+\\w+)?(<.*>)?(\\s+implements\\s+)?(.*)?(<.*>)?(\\s*))\\{$");

            regExpMap.put("EnumRegExp", "((((public|final|private|static|protected)(\\s+))?enum(\\s+)(\\w+)?(\\s+implements\\s+\\w+)?(.*)?\\s*))\\{$");


            Map<String, Pattern> patternsRegExpMap = new HashMap();
            for(String key : regExpMap.keySet()) {
                patternsRegExpMap.put("pattern" + key, Pattern.compile(regExpMap.get(key)));
            }


            for (String key : patternsRegExpMap.keySet()) {

            for (String path : folderPaths) {
                Pattern pattern = Pattern.compile("\\.java");
                Matcher matcher = pattern.matcher(path);
                boolean found = matcher.find();
                if (found) {
                    String result = null;

                    DataInputStream reader = new DataInputStream(new FileInputStream(path));
                    int nBytesToRead = reader.available();
                    if(nBytesToRead > 0) {
                        byte[] bytes = new byte[nBytesToRead];
                        reader.read(bytes);
                        result = new String(bytes);
                    }

                    //System.out.println(result);
                    String[] fileStrings = result.split("\n");


                    for (String str : fileStrings) {

                            Matcher m = patternsRegExpMap.get(key).matcher(str);
                            if (m.find()) {
                                if (key.contains("Class")) {
                                    res.get("class").add(m.group().replace(" {", ""));
                                    //System.out.println(m.group());
                                }
                                if (key.contains("Interface")) {
                                    res.get("interface").add(m.group().replace(" {", ""));
                                    //System.out.println(m.group());
                                }
                                if (key.contains("Enum")) {
                                    res.get("enum").add(m.group().replace(" {", ""));
                                    //System.out.println(m.group());
                                }
                            }
                        }
                    }
                }
            }

            for (String key : res.keySet()) {
                if(key.equals("class")) {
                    System.out.println("Classes\n");
                    for (String str : res.get(key)) {
                        System.out.println(str);
                    }
                }
                if(key.equals("interface")) {
                    System.out.println("\nInterfaces\n");
                    for (String str : res.get(key)) {
                        System.out.println(str);
                    }
                }
                if(key.equals("enum")) {
                    System.out.println("\nEnums\n");
                    for (String str : res.get(key)) {
                        System.out.println(str);
                    }
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }


    }



}
