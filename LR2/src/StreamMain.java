import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamMain {

    public static void main(String[] args) {
        String folderName = "C:\\Thumbtack\\thumbtack_online_school_2018_2_aleksandr_salenko-master";

        String classRegExp = "(((|public|final|abstract|private|static|protected)(\\s+))?(class)(\\s+)(\\w+)(<.*>)?(\\s+extends\\s+\\w+)?(<.*>)?(\\s+implements\\s+)?(.*)?(<.*>)?(\\s*))\\{$";

        String interfaceRegExp = "(((|public|final|abstract|private|static|protected)(\\s+))?interface(\\s+)(\\w+)(<.*>)?(\\s+extends\\s+\\w+)?(<.*>)?(\\s+implements\\s+)?(.*)?(<.*>)?(\\s*))\\{$";

        String enumRegExp = "((((public|final|private|static|protected)(\\s+))?enum(\\s+)(\\w+)?(\\s+implements\\s+\\w+)?(.*)?\\s*))\\{$";

        System.out.println("Classes\n");
        printFiles(folderName, classRegExp);

        System.out.println("\nInterfaces\n");
        printFiles(folderName, interfaceRegExp);

        System.out.println("\nEnums\n");
        printFiles(folderName, enumRegExp);
    }

    public static void printFiles(String folderName, String pattern) {
        try (Stream<Path> files = Files.walk(Paths.get(folderName))) {

            Pattern patternJava = Pattern.compile("\\.java");

            List<String> javaFiles = files.map(Path::toAbsolutePath).map(Path::toString).filter(patternJava.asPredicate()).collect(Collectors.toList());
            for (String file : javaFiles) {
                String result = null;

                DataInputStream reader = new DataInputStream(new FileInputStream(file));
                int nBytesToRead = reader.available();
                if (nBytesToRead > 0) {
                    byte[] bytes = new byte[nBytesToRead];
                    reader.read(bytes);
                    result = new String(bytes);
                }

                Pattern patternInterfaceRegExp = Pattern.compile(pattern);

                Arrays.stream(result.split("\n")).filter(patternInterfaceRegExp.asPredicate()).map(i -> i.replace(" {", "")).forEach(System.out::println);

            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


}
