import java.io.*;
import java.util.Arrays;
import java.util.regex.Pattern;

public class MyThread extends Thread{

    private String name;
    private String file;
    private String pattern;

    public MyThread(String threadName, String file, String pattern) {
        this.name = threadName;
        this.file = file;
        this.pattern = pattern;
    }

    public void run() {

        try{
            String result = null;

            DataInputStream reader = new DataInputStream(new FileInputStream(file));
                int nBytesToRead = reader.available();
                if (nBytesToRead > 0) {
                    byte[] bytes = new byte[nBytesToRead];
                    reader.read(bytes);
                    result = new String(bytes);
                }

                Pattern patternInterfaceRegExp = Pattern.compile(pattern);

                Arrays.stream(result.split("\n")).filter(patternInterfaceRegExp.asPredicate()).map(i -> i.replace(" {", "")).forEach(System.out::println);

        }
        catch(Exception e){
            if (e.getClass().equals(InterruptedException.class)) {
                System.out.println("Thread has been interrupted");
                e.printStackTrace();
            }
            if (e.getClass().equals(IOException.class)){
                System.out.println("Error occurred Input Output");
                e.printStackTrace();
            }
        }

    }

}
