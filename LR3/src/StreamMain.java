import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamMain {

    public static void main(String[] args) {
        //String folderName = "C:\\Thumbtack\\thumbtack_online_school_2018_2_aleksandr_salenko-master";

        String classRegExp = "(((|public|final|abstract|private|static|protected)(\\s+))?(class)(\\s+)(\\w+)(<.*>)?(\\s+extends\\s+\\w+)?(<.*>)?(\\s+implements\\s+)?(.*)?(<.*>)?(\\s*))\\{$";

        String interfaceRegExp = "(((|public|final|abstract|private|static|protected)(\\s+))?interface(\\s+)(\\w+)(<.*>)?(\\s+extends\\s+\\w+)?(<.*>)?(\\s+implements\\s+)?(.*)?(<.*>)?(\\s*))\\{$";

        String enumRegExp = "((((public|final|private|static|protected)(\\s+))?enum(\\s+)(\\w+)?(\\s+implements\\s+\\w+)?(.*)?\\s*))\\{$";

        String folderName = "E:\\Thumbtack\\thumbtack_sunday_school_2020_1_aleksandr_salenko";

        File directory = new File(folderName);
        // Убедимся, что директория найдена и это реально директория, а не файл.
        if (directory.exists() && directory.isDirectory()) {
            System.out.println("Classes\n");
            printFiles(folderName, classRegExp);

            System.out.println("\nInterfaces\n");
            printFiles(folderName, interfaceRegExp);

            System.out.println("\nEnums\n");
            printFiles(folderName, enumRegExp);
        } else {
            System.out.println("Не удалось найти директорию по указанному пути.");
        }

    }

    public static void printFiles(String folderName, String pattern) {
        try (Stream<Path> files = Files.walk(Paths.get(folderName))) {

            Pattern patternJava = Pattern.compile("\\.java");
            List<MyThread> myThreadList = new ArrayList();
            List<String> javaFiles = files.map(Path::toAbsolutePath).map(Path::toString).filter(patternJava.asPredicate()).collect(Collectors.toList());
            for (String file : javaFiles) {
                MyThread thread = new MyThread("File: " + file, file, pattern);
                myThreadList.add(thread);
                thread.start();
            }
            for (MyThread thread : myThreadList) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


}
