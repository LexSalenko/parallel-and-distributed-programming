import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class RunableMain {

    public static void main(String[] args) throws IOException {

        String folderName = "E:\\Education\\4Course\\Параллельное и распределенное программирование\\TestRepository\\spring-framework";
        File directory = new File(folderName);

        Pattern patternJava = Pattern.compile("\\.java");
        try (Stream<Path> files = Files.walk(Paths.get(folderName))) {

            List<String> javaFiles = files.map(Path::toAbsolutePath).map(Path::toString).filter(patternJava.asPredicate()).collect(Collectors.toList());
            String[] jf = new String[javaFiles.size()];
            jf = javaFiles.toArray(jf);
            File[] f = new File[javaFiles.size()];
            for (int i = 0; i < f.length; i++) {
                f[i] = new File(jf[i]);
            }
            new RunableMain().processDirectory(f);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }


    }

    private void processDirectory(File[] files) throws InterruptedException {

        Map<String, Set<String>> classMap = new HashMap<>();

        if (files == null) {
            System.out.println("Нет доступных файлов для обработки.");
            return;
        } else {
            System.out.println("Количество файлов для обработки: " +
                    files.length);
        }
        List<Thread> threads = new ArrayList<>();
        for (File file : files) {
            Runnable runnable = () -> {
                try {
                    Scanner sc = new Scanner(file);
                    while (sc.hasNext()) {
                        String[] strings = sc.nextLine().split(" ");
                        for (int i = 0; i < strings.length; i++) {
                            if (strings[i].equals("extends")) {
                                synchronized (this) {
                                    Set<String> orDefault =
                                            classMap.getOrDefault(strings[i + 1], new HashSet<>());
                                    orDefault.add(strings[i - 1]);

                                    classMap.put(strings[i + 1], orDefault);
                                }
                            }
                            if (strings[i].equals("implements")) {
                                synchronized (this) {
                                    Set<String> orDefault =
                                            classMap.getOrDefault(strings[i + 1], new HashSet<>());
                                    orDefault.add(strings[i - 1]);

                                    classMap.put(strings[i + 1], orDefault);
                                }
                            }
                        }
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            };
            Thread thread = new Thread(runnable);
            threads.add(thread);
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
        classMap.forEach((parent, children) -> System.out.println(parent + " ==> " + String.join(", ", children)));
    }
}