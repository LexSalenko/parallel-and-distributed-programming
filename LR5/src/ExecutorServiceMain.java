import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExecutorServiceMain {

    public static void main(String[] args) {

        String folderName = "E:\\Education\\4Course\\Параллельное и распределенное программирование\\TestRepository\\spring-framework";
        File directory = new File(folderName);

        Pattern patternJava = Pattern.compile("\\.java");
        try (Stream<Path> files = Files.walk(Paths.get(folderName))) {

            List<String> javaFiles = files.map(Path::toAbsolutePath).map(Path::toString).filter(patternJava.asPredicate()).collect(Collectors.toList());
            String[] jf = new String[javaFiles.size()];
            jf = javaFiles.toArray(jf);
            File[] f = new File[javaFiles.size()];
            for (int i = 0; i < f.length; i++) {
                f[i] = new File(jf[i]);
            }
            List<Future> futures = processDirectory(f);

            try {
                List<String> r = new ArrayList<>();
                for (Future future : futures) {
                    String temp_res = (String) future.get();
                    if (temp_res != null && temp_res.length() != 0) {
                        r.add(temp_res);
                    }
                }
                r.forEach(System.out::println);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static List<Future> processDirectory(File[] files) {


        if (files == null) {
            System.out.println("Нет доступных файлов для обработки.");
            return null;
        } else {
            System.out.println("Количество файлов для обработки: " + files.length);
        }

        // Непосредственно многопоточная обработка файлов.
        ExecutorService service = Executors.newFixedThreadPool(10);
        List<Future> futures = new ArrayList<>();
        for (final File f : files) {

            if (!f.isFile()) {
                continue;
            }

            Future future = service.submit(new Callable() {
                public Object call() throws Exception {
                    Map<String, Set<String>> classMap = new HashMap<>();
                    try {

                        Scanner sc = new Scanner(f);
                        while (sc.hasNext()) {
                            String[] strings = sc.nextLine().split(" ");
                            for (int i = 0; i < strings.length; i++) {
                                if (strings[i].equals("extends")) {

                                    Set<String> orDefault =
                                            classMap.getOrDefault(strings[i + 1], new HashSet<>());
                                    orDefault.add(strings[i - 1]);

                                    classMap.put(strings[i + 1], orDefault);

                                }
                                if (strings[i].equals("implements")) {
                                    Set<String> orDefault =
                                            classMap.getOrDefault(strings[i + 1], new HashSet<>());
                                    orDefault.add(strings[i - 1]);

                                    classMap.put(strings[i + 1], orDefault);

                                }
                            }
                        }

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    String result = "";
                    classMap.forEach((parent, children) -> System.out.println(parent + " ==> " + String.join(", ", children)));
                    //System.out.println(result);
                    return result;
                }
            });

            futures.add(future);
        }
        // Новые задачи более не принимаем, выполняем только оставшиеся.
        service.shutdown();
        // Ждем завершения выполнения потоков не более 10 минут.
        try {
            service.awaitTermination(10, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return futures;
    }


}
