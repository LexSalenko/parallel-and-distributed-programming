import java.io.*;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

public class BlockingQueueAndMapReduce {

    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter folder path:");
            String folderPath = reader.readLine();
            reader.close();
            File folder = new File(folderPath);
            if (folder.exists() && folder.isDirectory()) {
                processDirectory(folder);
            } else {
                System.out.println("Folder does not exist.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processDirectory(File directory) {
        FileFilter filter = directory1 ->
                directory1.getName().endsWith("java");
        File[] files = directory.listFiles(filter);
        if (files == null) {
            System.out.println("Folder is empty.");
            return;
        }
        ArrayBlockingQueue<File> filesForJob = new ArrayBlockingQueue<>(10);
        ArrayBlockingQueue<Map<String, Set<String>>> result = new
                ArrayBlockingQueue<>(10);
        HashMap<String, Set<String>> poisonPill = new HashMap<>();
        poisonPill.put("poisonPill", new HashSet<>());
        Producer producer = new Producer(filesForJob, files);
        Consumer handler = new Consumer(filesForJob, result, poisonPill);
        ResultPutAndPrint print = new ResultPutAndPrint(result, poisonPill);
        Thread prod = new Thread(producer);
        Thread handle = new Thread(handler);
        Thread prin = new Thread(print);
        prod.start();
        handle.start();
        prin.start();
    }
}

class Producer implements Runnable {
    ArrayBlockingQueue<File> filesForWork;
    File[] files;

    public Producer(ArrayBlockingQueue<File> filesForWork, File[] files) {
        this.filesForWork = filesForWork;
        this.files = files;
    }

    public void run() {
        for (File f : files) {
            try {
                filesForWork.put(f);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}

class Consumer implements Runnable {
    ArrayBlockingQueue<File> filesForWork;
    ArrayBlockingQueue<Map<String, Set<String>>> result;
    Map<String, Set<String>> classMap = new ConcurrentHashMap<>();
    Map<String, Set<String>> poisonPill;

    Consumer(ArrayBlockingQueue<File> filesForWork,
             ArrayBlockingQueue<Map<String, Set<String>>> result, Map<String, Set<String>>
                     poisonPill) {
        this.filesForWork = filesForWork;
        this.result = result;
        this.poisonPill = poisonPill;
    }

    public void run() {
        try {
            while (true) {
                try {
                    Scanner sc = new Scanner(filesForWork.take());
                    while (sc.hasNext()) {
                        String[] strings = sc.nextLine().split(" ");
                        for (int i = 0; i < strings.length; i++) {
                            if (strings[i].equals("extends")) {
                                Set<String> orDefault =
                                        classMap.getOrDefault(strings[i + 1],
                                                new HashSet<>());
                                orDefault.add(strings[i - 1]);
                                classMap.put(strings[i + 1], orDefault);
                            }
                            if (strings[i].equals("implements")) {
                                Set<String> orDefault =
                                        classMap.getOrDefault(strings[i + 1],
                                                new HashSet<>());
                                orDefault.add(strings[i - 1]);
                                classMap.put(strings[i + 1], orDefault);
                            }
                        }
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                result.put(classMap);
                if (filesForWork.size() == 0) {
                    result.put(poisonPill);
                    break;
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}

class ResultPutAndPrint implements Runnable {
    ArrayBlockingQueue<Map<String, Set<String>>> result;
    HashMap<String, Set<String>> map = new HashMap<>();
    Map<String, Set<String>> poisonPill;

    public ResultPutAndPrint(ArrayBlockingQueue<Map<String, Set<String>>> result, Map<String, Set<String>> poisonPill) {
        this.result = result;
        this.poisonPill = poisonPill;
    }

    @Override
    public void run() {
        try {
            boolean end = true;
            do {
                Map<String, Set<String>> taken = result.take();
                if (taken.equals(poisonPill)) {
                    end = false;
                } else {
                    map.putAll(taken);
                }
            } while (end);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        map.forEach((parent, children) -> System.out.println(parent + " ==> " + String.join(", ", children)));
    }
}

